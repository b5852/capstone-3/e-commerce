import {useState, useEffect, useContext} from "react";
import UserContext from "../UserContext";
import {Navigate, useNavigate} from "react-router-dom";

import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import {Button, Form} from "react-bootstrap";


import Login from "../pages/Login";


export default function Register(){

	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	//State hooks to store the values of the input fields
	const [completeName, setCompleteName] = useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// Function to simulate user registration
	function registerUser(e){
		//prevents the page redirection via form submit
		e.preventDefault();


				fetch(`${process.env.REACT_APP_API_URL}/users/signup`, {
					method: "POST",
					headers:{
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						completeName: completeName,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data){
						//Clear input fields
						setCompleteName("");
						setEmail("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Welcome to E-Shop!"
						})

						//redirect the user to the login page after registration.
						navigate("/login");
					}
					else if(data === 'exist'){
						Swal.fire({
							title: "Email exist",
							icon: "success",
							text: "Email already exist!"
						})
					}
					else{
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			
		}

	//State to determine whether submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);

	// To enable the submit button:
		// No empty input fields.
		// password and verify password should be the same.

	useEffect(()=>{
		if((completeName !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	},[completeName, email, password1, password2])


	return(
		(user.id !== null)
		?
			<Navigate to="/products"/>
		:
		<>
			<div className="bg-image">
				<div className="Auth-form-container">
			      <form className="Auth-form" onSubmit ={(e) => registerUser(e)}>
			        <div className="Auth-form-content">
			          <h3 className="Auth-form-title">Create new account</h3>
			          <div className="form-group mt-3">
			            <label>Complete Name</label>
			            <input
			              type="text"
			              className="form-control mt-1"
			              placeholder="Enter your complete name"
			              value={completeName} onChange={e => setCompleteName(e.target.value)}
			            />
			          </div>
			          <div className="form-group mt-3">
			            <label>Email address</label>
			            <input
			              type="email"
			              className="form-control mt-1"
			              placeholder="Enter email"
			              value={email} onChange={e => setEmail(e.target.value)}
			            />
			          </div>
			          <div className="form-group mt-3">
			            <label>Password</label>
			            <input
			              type="password"
			              className="form-control mt-1"
			              placeholder="Enter password"
			              value={password1} onChange={e => setPassword1(e.target.value)}
			            />
			          </div>
			          <div className="form-group mt-3">
			            <label>Verify Password</label>
			            <input
			              type="password"
			              className="form-control mt-1"
			              placeholder="Enter password"
			              value={password2} onChange={e => setPassword2(e.target.value)}
			            />
			          </div>
			          <div className="d-grid gap-2 mt-3">
			            {
	        		      	isActive
	        		      	?
	        		      		<Button variant="primary" type="submit" id="submitBtn">
	        		      		  Submit
	        		      		</Button>
	        		      	:
	        		      		<Button variant="primary" type="submit" id="submitBtn" disabled>
	        		      		  Submit
	        		      		</Button>
	        		    }
			          </div>
			          <p className="forgot-password text-right mt-4">
			            <Link as={Link} to="/login">I already have an account.</Link>
			          </p>
			        </div>
			      </form>
			    </div>
			</div>

		</>
	)
}