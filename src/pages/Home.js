import Banner from "../components/Banner";
import Highlights from "../components/Highlights";

export default function Home(){
	const data = {
		title: "The best way to shop online!",
		content: "Stay connected, worry-free and pay secure to our site. Best and powerful platform for online shopping with better and faster shipping",
		destination: "/products",
		label: "Shop now!"
	}

	return(
		<>
			<Banner data={data}/>
        	<Highlights />
		</>
	)
}