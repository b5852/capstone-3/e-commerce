//Applying bootstrap grid system
import {Row, Col, Card, Container} from "react-bootstrap";

export default function Highlights(){
	return(

	    <Container>
	    	<Row className="p-5 text-center justify-content-md-center">
				<h1 className="header-title">EXPLORE OUR PRODUCT</h1>
			</Row>
			<Row className="mt-3 mb-3">
			    <Col className="col-12 col-md-6 col-lg-4 mb-5">
			    	<Card className="cardHighlight">
			    		<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=1rwXUS80qPeiiKZa_HgZQPcTvdGdbqdOH`}  />
				        <Card.Body className="productCatalog">
			                <div className="text">
			                    <h5></h5>
			                    <h3>Headphones</h3>
			                    <div className="description">
			                        <p>-Beat Response Control for tight bass
										-Swivel design makes travel easy
										-In-line remote and mic for hands-free calls
										-Aluminium fascia for punchy bass
										-Neodymium dynamic drivers deliver precise sound
										-Cushioned earpads for total comfort
										-High sensitivity for a loud, clear sound
										-Enfolding closed-back design seals in sound
										-Wide frequency response for clear highs and lows
										-Cable Approx.1.2m
									</p>
			                    </div>
			                </div>
				        </Card.Body>
				      </Card>
				</Col>
				<Col className="col-12 col-md-6 col-lg-4 mb-5">
			    	<Card className="cardHighlight">
			    		<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=1uMgGUELqGdBP_ONjDVRNd58iWpZ2Rswn`}  />
				        <Card.Body className="productCatalog">
			                <div className="text">
			                    <h5></h5>
			                    <h3>Keyboard and Mouse</h3>
			                    <div className="description">
			                        <p>-•Enjoy a comfortable and quiet typing experience thanks to the low-profile keys that barely make a sound and standard layout with full-size F-keys and number pad.
										•Slim keyboard isn’t just sleek—it’s tough with a spill-resistant design, sturdy tilt legs and durable keys
										•moving along smoothly with a precise, high-definition optical mouse to help you get around.
										•Just plug your keyboard and mouse into USB ports on your desktop, laptop or netbook computer and start using them right out of the box.
										•Zeus Guarantee You will get the high quality and reliability Products
									</p>
			                    </div>
			                </div>
				        </Card.Body>
				      </Card>
				</Col><Col className="col-12 col-md-6 col-lg-4 mb-5">
			    	<Card className="cardHighlight">
			    		<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=1iB3qWJmTSFuf3Gbn_KMCQE3yV_o3SbYU`}  />
				        <Card.Body className="productCatalog">
			                <div className="text">
			                    <h5></h5>
			                    <h3>Headphones</h3>
			                    <div className="description">
			                        <p>10th Gen Intel® Celeron N5095 processor up to 2.9GHz, 15.6" laptop with full-size keyboard, numeric keypad for easy typing, multiple color and storage configuration options to meet your every need
										Fast and stable network 2.6G WiFi 1.0 GPS RJ45, smoother movie watching, no delay in playback
										The 15.6-inch high-definition display with a wide viewing angle and a full screen allows the entire screen to have a higher screen ratio, providing vivid visual effects and a wider visual experience.
									</p>
			                    </div>
			                </div>
				        </Card.Body>
				      </Card>
				</Col>
			</Row>
	    </Container>
	)
}