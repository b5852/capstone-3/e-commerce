import React from "react";
import { Link } from "react-router-dom";
import { Card, Button, Col, Stack} from "react-bootstrap";
import { useCart } from "react-use-cart";

import Swal from "sweetalert2"

import { useState, useEffect } from "react"

import '../style/product.css';

const ProductCard = (props) => {

	const { addItem } = useCart()
	const [myProduct, setMyProduct] = useState()


	function addToCart(myProduct){
			Swal.fire({
					title: "Success",
					icon: "success",
					text: "Add to cart successfully!"
				})
			addItem(myProduct)
		}


	useEffect(() => {
	    setMyProduct({
	      	id:props.product._id,
	      	name: props.title,
	      	description:props.description,
	      	price:props.price
	    });
	}, []);

	return (
		<>
				<Col className="col-12 col-md-6 col-lg-4 mb-5">
			    	<Card className="cardHighlight">
			    		<Card.Img variant="top" src={`https://drive.google.com/uc?export=view&id=${props.product.imageURL}`}  />
				        <Card.Body className="productCatalog">
			                <div className="text">
			                    <h5></h5>
			                    <h3>{props.title}</h3>
			                    <div className="price">
			                        <h5 className="text-danger">₱ {props.price}</h5>
			                        {
			                            props.product.stocks > 0
			                            ? <h5 className="text-danger">In Stock: {props.product.stocks}</h5>
			                            : <h5 className="text-danger">Out Stock</h5>
			                        }
			                    </div>
			                    <div className="description">
			                        <h5>Description</h5>
			                        <p>{props.description}</p>
			                    </div>
			                </div>
				        </Card.Body>
				        <div className="d-grid gap-2">
				        	{
				        	    props.product.stocks > 0
				        	    ? 
				        	    	<>
				        	    		<Button as={Link} to={`/products/${props.product._id}`} variant="primary">View</Button>
				        	    		<Button className="btn btn-success" onClick={() => addToCart(myProduct)}>Add to Cart</Button>
				        	    	</>
				        	    : 
				        	    	<>
				        	    		<Button as={Link} to={`/products/${props.product._id}`} variant="primary">View</Button>
				        	    		<Button className="btn btn-danger" disabled>Add to Cart</Button>
				        	    	</>
				        	}
						</div>
				      </Card>
				</Col>

		</>
		
			
		);
	}
export default ProductCard
