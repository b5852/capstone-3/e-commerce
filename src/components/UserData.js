import { useContext, useState, useEffect } from "react";
import { Modal, Button, Tooltip, Form } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
import Swal from "sweetalert2"

export default function UserData({userList}) {


	const { userId } = useParams()

    const [isAdmin, setIsAdmin] = useState(userList.isAdmin);
    

	// User setAsCustomer function 
	const setCustomer = (userList) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/setUserAdmin/${userList._id}`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				_id: userId,
				isAdmin: false
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {

				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${userList.completeName} set as customer successfully`
				})
				setIsAdmin(false)

			}
			else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: `Please try again`
				})

			}
		})
	}

	// User setAsAdmin function 
	const setAdmin = (userList) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/setUserAdmin/${userList._id}`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				_id: userId,
				isAdmin: true
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data) {

				Swal.fire({
					title: "Success!",
					icon: "success",
					text: `${userList.completeName} set as admin successfully`
				})
				setIsAdmin(true)

			}
			else {
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: `Please try again`
				})

			}
		})
	}



    return (
    	 <>
    	 	    <td>{userList._id}</td>
			    <td>{userList.completeName}</td>
			    <td>{userList.email}</td>

				    {
				    	(isAdmin)
				    	?
				    	<>
						    <td>Yes</td>
				    		<td><Button onClick={() => setCustomer(userList)} variant="success">Set as Customer</Button></td>
				    	</>
				    	:
				    	<>
						    <td>No</td>
				    		<td><Button onClick={() => setAdmin(userList)} variant="primary">Set as Customer</Button></td>
				    	</>
				    }
    	    </>
    )

}