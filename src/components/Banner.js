import {Row, Col, Button, Container, Image} from "react-bootstrap";
import {Link} from "react-router-dom";

export default function Banner({data}){
	console.log(data);

	const {title, content, destination, label} = data;

	return(
		<header id="header" className="header" expand="lg">
			<Container fluid>
				<Row >
					<Col lg="6" xl="6" className="pb-5 text-center">
						<h1>{title}</h1>
						<p>{content}</p>
						<Button as={Link} to={destination} variant="primary">{label}</Button>
					</Col>
					<Col lg="6" xl="6" className="text-center">
						<Container className="image-container">
							<img src="./img/1.png" className="img-fluid" alt="Alternative" />	
						</Container>
					</Col>
				</Row>
			</Container>
		</header>
		
	)
}