
import { Link } from "react-router-dom";
import {useState, useContext} from "react";
import UserContext from "../UserContext"

import {Navbar, Nav, Container, NavDropdown} from "react-bootstrap";

export default function AppNavbar(){

	// We consume te "user" value from the User context to update the Nav links in our navigation bar.
	const {user} = useContext(UserContext);

	return(
		<Navbar bg="dark" variant="dark" expand="lg">
	      <Container>
	        <Navbar.Brand as={Link} to="/" >E-SHOP</Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav" />
	        <Navbar.Collapse id="basic-navbar-nav">
	    		{/*className is use instead of "class", to specify a CSS classes*/}
	          <Nav className="ms-auto" defaultActiveKey="/">
	            <Nav.Link as={Link} to="/" eventKey="/">Home</Nav.Link>
	            {/*
	            	Before applying useContext hooks:
						user.email is causing an error when the local storage is cleared because the initial state of user is change to null and we can't perform a dot notation to a non-object datatype.
	            */}
	            {
	            	(user.isAdmin)
	            	?
	            		<Nav.Link as={Link} to="/admin" eventKey="/admin">Admin Dashboard</Nav.Link>
	            	:
	            		<>
	            			<Nav.Link as={Link} to="/products" eventKey="/products">Product</Nav.Link>
	            			<Nav.Link as={Link} to="/cart" eventKey="/cart">Cart</Nav.Link>
	            		</>
	            }

	            {	
	            	(user.id !== null)
	            	?
	            		<Nav.Link as={Link} to="/logout" eventKey="/logout">Logout</Nav.Link>
	            	:
	            		<>
		            		<Nav.Link as={Link} to="/login" eventKey="/login">Login</Nav.Link>
		            		<Nav.Link as={Link} to="/register" eventKey="/register">Register</Nav.Link>
	            		</>
	            }
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}