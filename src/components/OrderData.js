import { useContext, useState, useEffect } from "react";
import { Modal, Button, Tooltip, Form } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom"
import Swal from "sweetalert2"

export default function OrderData({userOrders}) {



    const [orderId, setOrderId] = useState(userOrders._id);
    const [userId, setUserId] = useState(userOrders.userId);

    const [orders, setOrders] = useState([]);

    
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/profile/${userId}`,
        {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem("token")}`
                }
        })
        .then(res => res.json())
        .then(data => { setOrders(data.completeName) 
        })
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
    	<>
            {orders}
    	</>
    )

}